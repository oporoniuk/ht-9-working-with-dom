// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
//це структурне дерево в html. Тобто, все, що ми маємо в своєму html: 
// - HTML-елементи як об'єкти
// - Властивості цих HTML-елементів 
// - Методи доступу до тих же елементів.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//Властивість innerHTML дозволяє отримувати HTML всередині елемента у вигляді рядка.
//Ми також можемо його змінювати. Таким чином, це один із найпотужніших способів змінювати сторінку.
//Використовуйте `innerHTML`, коли потрібно маніпулювати HTML-вмістом всередині елемента.
// Використовуйте `textContent`, коли хочете маніпулювати текстовим вмістом без врахування HTML-тегів.
// Використовуйте `innerText`, коли потрібно маніпулювати текстовим вмістом з урахуванням стилізації CSS та можливого виключення прихованих елементів.


// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//getElementById, getElementsByTagName, getElementsByClassName, getElementsByName,
//querySelector, querySelectorAll
//getElementsBy - повертають колекції, 
//querySelector - повертає перший елемент з відповідним селектором
//querySelectorAll  - повертає всі елементи з відповідним селектором
//getElementById - повертає елемент з відповідним ID, 
//
// Загальні принципи:

// Специфічність:
// Методи типу `getElement` високоспецифічні, надаючи прямий шлях до певного елемента. 
//Проте для їх використання необхідні унікальні ідентифікатори.
// Методи типу `querySelector` забезпечують баланс між специфічністю та гнучкістю, 
//дозволяючи вибирати елементи за допомогою складних селекторів.

// Продуктивність:
// Методи типу `getElement` зазвичай мають кращу продуктивність завдяки своїй специфічності та 
//прямому доступу до елементів.
// Методи типу `querySelector` можуть бути трошки повільнішими, особливо при роботі з складними селекторами, 
//оскільки вони вимагають більше обробки.
// Компроміс між Гнучкістю та Продуктивністю:

// 4. Яка різниця між nodeList та HTMLCollection?
// nodeList - надає всі типивузлів (прості і складні)
//HTMLCollection специфічно стосується HTML елементів







// Практичні завдання
// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.

const featureElements = document.querySelectorAll('.feature');
console.log(featureElements);

const featureEl = Array.from(document.getElementsByClassName('feature'));
console.log(featureEl);
featureEl.forEach(p => p.setAttribute('style', 'text-align: center'));


// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).


// 2. Змініть текст усіх елементів h2 на "Awesome feature".
const awesomeHeadings = Array.from(document.getElementsByTagName('h2'));
console.log(awesomeHeadings);
awesomeHeadings.forEach(h2 => h2.innerText = 'Awesome Feature');



// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
const featureTitle = Array.from(document.querySelectorAll('.feature-title'));
console.log(featureTitle);

function addExlamation() {
    const Exclamation = document.createElement('span');
    Exclamation.textContent = '!';
    return Exclamation;
}


featureTitle.forEach(h3 => h3.append(addExlamation()));
console.log(featureTitle);
